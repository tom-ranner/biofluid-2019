HTML_TARGETS=index.html

TARGETS=${HTML_TARGETS}

BUILDDIR=public

all: ${TARGETS}

%.html: %.md
	pandoc --filter pandoc-citeproc \
	--mathjax \
	--template=./revealjs-template.html \
	-t revealjs \
	--slide-level 1 \
	-V controls=true \
	-V controlsTutorial=true \
	-V slideNumber="'c'" \
	-s $< -o $@

%.embed.html: %.md
	pandoc --filter pandoc-citeproc \
	--mathjax \
	--template=./revealjs-template.html \
	-t revealjs \
	-V controls=true \
	-V controlsTutorial=true \
	-V slideNumber=false \
	--slide-level 1 \
	-V embedded="true" \
	-s $< -o $@

%.full.html: %.md
	pandoc --filter pandoc-citeproc \
	--self-contained \
	--mathjax \
	--template=./revealjs-template.html \
	-t revealjs \
	--slide-level 1 \
	-V controls=false \
	-V slideNumber="'c'" \
	-s $< -o $@

build: ${HTML_TARGETS}
	mkdir -p ${BUILDDIR}
	cp ${HTML_TARGETS} ${BUILDDIR}/.
	cp -r css ${BUILDDIR}/css
	cp -r img ${BUILDDIR}/img
	cp -r js ${BUILDDIR}/js
	cp -r video ${BUILDDIR}/video

clean:
	rm -f ${TARGETS}
	rm -rf ${BUILDDIR}
