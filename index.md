---
author:
- name: Tom Ranner
  address: School of Computing, University of Leeds, UK
  email: T.Ranner@leeds.ac.uk
title: Robust computational approaches to free boundary problem
# pandoc options
revealjs-url: ./js/reveal.js
transition: none
autoPlayMedia: true
css: css/metropolis.css
date: School of Computing
slides: Slides available at <a href="https://tomranner.org/talk/biofluids">tomranner.org/talk/biofluids</a>
# mathjax
mathjaxurl: ./js/mathjax/es5/tex-chtml-full.js
include-before: |
  <div style="display:none">
  </div>
---

# Classical examples of free boundary problems

<div class="container">
<div class="col">

<video data-src="video/hele-shaw.mp4" controls="" loop="" autoplay="" width="80%"><a href="video/hele-shaw.mp4">Video</a></video>

Hele-Shaw cell
("squeezed" fluid)

<small>
[vimeo.com/22212386](https://vimeo.com/22212386)
</small>
</div>

<div class="col">

<video data-src="video/stefan-problem.mp4" controls="" loop="" autoplay="" width="80%"><a href="video/stefan-problem.mp4">Video</a></video>

Stefan problem
(crystal growth)

<small>
[youtube.com/watch?v=9qD88At6oVY](https://www.youtube.com/watch?v=9qD88At6oVY)
</small>
</div>
</div>


# More biological examples

<div class="container">
<div class="col">
<video data-src="video/dalous.h264.mp4" controls="" loop="" autoplay="" width="80%"><a href="video/dalous.h264.mp4">Video</a></video>

Cell motility

<small>
Dalous et al. 2008. [doi:10.1529/biophysj.107.114702](https://doi.org/10.1529/biophysj.107.114702)
</small>
<small>
For modelling/simluation see e.g. </br>
Elliott, Stinner, Venkataraman 2012. [doi:10.1098/rsif.2012.0276](https://doi.org/10.1098/rsif.2012.0276), </br>
Murphy, Madzvamuse 2019 [arxiv:1903.09535](https://arxiv.org/abs/1903.09535)
</small>
</div>

<div class="col">
<video data-src="video/blood-flow.mp4" controls="" loop="" autoplay="" width="80%"><a href="video/blood-flow.mp4">Video</a></video>

Cardiovascular blood flow

<small>
Simulation: Marsden 2015.
[url](https://sinews.siam.org/Details-Page/cardiovascular-blood-flow-simulation)
</small>
</div>
</div>

# Approach

**Analysis based computational modelling**

![](./img/flowchart.svg){ class="stretch plain" }

# Separation of ligand receptor regions on cell boundary

![](./img/z_5.jpg){ width="20%" }
![](./img/z_20.jpg){ width="20%" }
![](./img/z_40.jpg){ width="20%" }
![](./img/z_60.jpg){ width="20%" }

<small>
Elliott, TR, Venkataraman 2017. [doi:10.1137/15M1050811](https://doi.org/10.1137/15M1050811)
</small>

# Caenorhabditis elegans crawling in 2d and 3d

<video data-src="video/4_00pc_trial02_curvature-3d.webm" controls="" loop="" autoplay="" width="45%"><a href="video/4_00pc_trial02_curvature-3d.webm">Video</a></video>

<video data-src="video/locomotion-2d.h264.mp4" controls="" loop="" autoplay="" width="45%"><a href="video/locomotion-2d.h264.mp4">Video</a></video>
<video data-src="video/locomotion-3d.h264.mp4" controls="" loop="" autoplay="" width="45%"><a href="video/locomotion-3d.h264.mp4">Video</a></video>

<small>
TR 2019. [arxiv:1904.01325](https://arxiv.org/abs/1904.01325)
</small>

# 

**Thank you for your attention**

<small>
Slides available at <a href="https://tomranner.org/talk/biofluids">tomranner.org/talk/biofluids</a>
</small>

# References

<div style="text-align:left,margin-left:2em;text-indent: -2em;">
<small>
J. Dalous, E. Burghardt, A. Müller-Taubenberger, F. Bruckert, G. Gerisch, T. Bretschneider 2008.
*Reversal of Cell Polarity and Actin-Myosin Cytoskeleton Reorganization under Mechanical and Chemical Stimulation*.
Biophys. J.
94:3.
[doi:10.1529/biophysj.107.114702](https://doi.org/10.1529/biophysj.107.114702)

C.M. Elliott, B. Stinner, C. Venkataraman 2012.
*Modelling cell motility and chemotaxis with evolving surface finite elements*.
J. R. Soc. Interface, 9.
[doi:10.1098/rsif.2012.0276](https://doi.org/10.1098/rsif.2012.0276)

C.M. Elliott, TR, C. Venkataraman 2017.
*Coupled Bulk-Surface Free Boundary Problems Arising from a Mathematical Model of Receptor-Ligand Dynamics*.
SIAM J. Math. Anal., 49(1), 360–397.
[doi:10.1137/15M1050811](https://doi.org/10.1137/15M1050811)

A.L. Marsden 2015.
SIAM News. December 2015. online.
[sinews.siam.org/Details-Page/cardiovascular-blood-flow-simulation](https://sinews.siam.org/Details-Page/cardiovascular-blood-flow-simulation)

L. Murphy, A. Madzvamuse 2019.
*A moving grid finite element method applied to a mechanobiochemical model for 3D cell migration*.
<!-- Preprint. -->
[arxiv:1903.09535](https://arxiv.org/abs/1903.09535)

TR 2019.
*A stable finite element method for an open, inextensible, viscoelastic rod.*
<!-- Preprint. -->
[arxiv:1904.01325](https://arxiv.org/abs/1904.01325)
</small>
</div>
